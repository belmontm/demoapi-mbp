##Imagen de la que parto
FROM node:boron
##Directorio de trabajo
WORKDIR /mbapp
##Copia archivos
ADD . /mbapp
##Paquetes necesarios
RUN npm install
##Puerto en que expongo
EXPOSE 3000
##Comando de inicio
CMD ["npm", "start"]
