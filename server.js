console.log("Aquí funcionando con Nodemon")

var movimientosJSON = require('./movimientosv2.json')
var usuariosJSON = require('./usuarios.json')
var express = require('express')
var bodyparser = require('body-parser')
var jsonQuery = require('json-query')
var requestJson = require('request-json')


var app = express()
app.use(bodyparser.json())

app.get('/', function(req, res) {
  res.send('Hola API')
})

app.get('/v1/movimientos', function(req, res) {
  res.sendfile('movimientosv1.json')
})

app.get('/v2/movimientos', function(req, res) {
  res.send(movimientosJSON)
})

app.get('/v2/movimientos/:id', function(req, res) {
  //console.log(req)
  console.log(req.params.id)
  //console.log(movimientosJSON)
  //res.send("Hemos recibido su petición de consulta del movimiento #" + req.params.id)
  res.send(movimientosJSON[req.params.id-1])
})

app.get('/v2/movimientosq', function(req, res) {
  console.log(req.query)
  res.send("recibido")
})

app.get('/v2/movimientosp/:id/:nombre', function(req, res) {
  console.log(req.params)
  res.send("recibido")
})

app.post('/v2/movimientos', function(req, res) {
  //console.log(req)
  //console.log(req.headers['authorization'])
  //if (req.headers['authorization']!=undefined)
  //{
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
  //}
  //else {
  //  res.send("No está autorizado")
  //}
})

app.put('/v2/movimientos/:id', function(req, res) {
  var cambios = req.body
  var actual = movimientosJSON[req.params.id-1]
  if (cambios.importe != undefined)
  {
    actual.importe = cambios.importe
  }
  if (cambios.ciudad != undefined)
  {
    actual.ciudad = cambios.ciudad
  }
  res.send("Cambios realizados")
})

app.delete('/v2/movimientos/:id', function(req, res) {
  var actual = movimientosJSON[req.params.id-1]
  movimientosJSON.push({
    "id": movimientosJSON.length+1,
    "ciudad": actual.ciudad,
    "importe": actual.importe * (-1),
    "concepto": "Negativo del " + req.params.id
  })
  res.send("Movimiento anulado")
})



app.get('/v2/usuarios/:id', function(req, res) {
  //console.log(req)
  console.log(req.params.id)
  //console.log(movimientosJSON)
  //res.send("Hemos recibido su petición de consulta del movimiento #" + req.params.id)
  res.send(usuariosJSON[req.params.id-1])
})

//=============================
// Recupera las colecciones en mLab que estàn en  MongoDB
//=============================

// Vesriòn 3 conectada a Mlab
var urlMlabRaiz ="https://api.mlab.com/api/1/databases/techumx-mb/collections"
var apiKey = "apiKey=NWxduBELGqbk_0LCvNB8HvCD1MRTwLIv"
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey)

app.get('/v3', function(req, res) {

    clienteMlab.get('',function(err,resM,body){
    var coleccionesUsuario = []
    if(!err){
      for (var i = 0; i < body.length; i++) {
        if (body[i] != 'system.indexes')
        {
          // Aquì lo que se hace es indicar los metodos con los que contamos en JSON
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/"+body[i]})
        }
      }
     res.send(coleccionesUsuario)
    }
    else {
     res.send(err)
    }
  })
})

//=============================
// Consulta de usuarios
//=============================
app.get('/v3/usuarios', function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.get('', function(err, resM, body){
       res.send(body)
    })
})


//=============================
// alta de usuarios
//=============================
app.post('/v3/usuarios', function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.post('', req.body, function(err, resM, body){
       res.send(body)
    })
})

//=============================
// Consulta por dtos especificos
//=============================
app.get('/v3/usuarios/:id', function(req, res)  {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")

    clienteMlab.get('?&q={"id":' + req.params.id + '}&' + apiKey, function(err, resM, body){
       res.send(body)
    })
})

//=============================
// Modificar datos
//=============================
app.put('/v3/usuarios/:id', function(req, res)  {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")

    var cambio = '{ "$set": {} }' + JSON.stringify(req.body) + '}'

    clienteMlab.put('?q={"id":' + req.params.id + '}&' + apiKey,
                    JSON.parse(cambio), function(err, resM, body){
       res.send(body)
    })
})


//=============================
// Consulta de movimientos
//=============================
app.get('/v3/movimientos', function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos?" + apiKey)
    clienteMlab.get('', function(err, resM, body){
       res.send(body)
    })
})

//=============================
// alta
//=============================
app.post('/v3/movimientos', function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos?" + apiKey)
    clienteMlab.post('', req.body, function(err, resM, body){
       res.send(body)
    })
})

//=============================
// Consulta por dtos especificos
//=============================
app.get('/v3/movimientos/:id', function(req, res)  {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos")

    clienteMlab.get('?&q={"id":' + req.params.id + '}&' + apiKey, function(err, resM, body){
       res.send(body)
    })
})

//=============================
// Modificar datos
//=============================
app.put('/v3/usuarios/:id', function(req, res)  {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos")

    var cambio = '{ "$set": {} }' + JSON.stringify(req.body) + '}'

    clienteMlab.put('?q={"id":' + req.params.id + '}&' + apiKey,
                    JSON.parse(cambio), function(err, resM, body){
       res.send(body)
    })
})


app.listen(3000)
console.log("Escuchando en el puerto 3000")
